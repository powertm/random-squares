import React from 'react';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.handleSquareClick = this.handleSquareClick.bind(this);

    this.state = {
      score: 0
    }
  }

  componentDidMount() {
    
  }

  componentDidUpdate() {
  }

  handleSquareClick() {
    this.setState( (state, props) => ({
      score: state.score + 1
    }))
  }

  render() {  
    return (
      <div className="App">
        <Score currentScore={ this.state.score }/>
        <Grid squareClick={ () => this.handleSquareClick() }/>
      </div>
    );
  }

}

class Grid extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rowLength: 20,
      time: this.generateRandom(3)
    }
  }

  componentDidMount() {
    this.refreshGrid();
  }

  refreshGrid() {
    setTimeout( () => {
      this.refreshGrid();
      this.setState({
        time: this.generateRandom(3)
      })
    }, this.state.time )
  }

  generateRandom(numSeconds) {
    return ( Math.ceil( Math.random() * Math.floor( numSeconds ) ) * 1000);
  }

  render() {

    let squares = [];

    const row = Math.floor( Math.random() * Math.floor( this.state.rowLength - 1 ) ),
          col = Math.floor( Math.random() * Math.floor( this.state.rowLength - 1 ) );

    for ( let i = 0 ; i < this.state.rowLength ; i++ ) {

      let currentSquaresRow = [];

      for ( let o = 0 ; o < this.state.rowLength ; o++ ) {

        let squareClass = 'square';

        if ( i === row && o === col ) {
          squareClass = 'square-exposed';
        }

        currentSquaresRow.push( <Square key={(i+o)} onClick={ () => this.props.squareClick() } squareClass={ squareClass } /> );

      }

      squares.push( <Row key={i}>{ currentSquaresRow }</Row> );
    }

    return <div id="grid">{ squares }</div>
  }
}

function Square(props) {

  function handleClick(e) {

    if ( e.target.classList.contains('square-exposed') ) {
      props.onClick();
    }
  }

  return <div onClick={ (e) => handleClick(e) } className={ props.squareClass }></div>
}

function Row(props) {
  return <div className="row">{ props.children }</div>
}

function Score(props) {
  return (
    <div id="score">
      <h1>Your score: {props.currentScore}</h1>
    </div>
  )
}



export default App;
